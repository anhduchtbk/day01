<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link rel="stylesheet" href="../style.css" />
    <script src="https://code.jquery.com/jquery-3.6.0.min.js"></script>
    <script src="https://cdn.jsdelivr.net/jquery.validation/1.16.0/jquery.validate.min.js"></script>
</head>

<body>
    <?php
    include "./image_process.php";

    ?>
    <div class="w-screen h-screen px-[30%] py-[10%]">
        <form method="POST" id="myform" action="data_process.php" enctype="multipart/form-data">
            <div class="border-[1px] border-[#5e87ab] p-[5%] space-y-2 w-full">
                <div class="flex flex-row space-x-3 w-full">
                    <div class="bg-[#70AD47] p-[2%] border-[1px] border-[#5e87ab] w-[30%]">
                        <label class="text-white">Họ và tên </label>
                    </div>
                    <div class='flex items-center justify-center'>
                        <label>
                            <?php echo $_POST['fullname'] ?>
                        </label>
                        <input type="hidden" name="fullname" value="<?php echo $_POST['fullname']; ?>">
                    </div>
                </div>

                <div class="flex flex-row space-x-3 w-full">
                    <div class="bg-[#70AD47] p-[2%] border-[1px] border-[#5e87ab] w-[30%]">
                        <label class="text-white">Giới tính </label>
                    </div>
                    <div class='flex items-center justify-center'>
                        <label>
                            <?php echo $_POST['gender'] ?>
                        </label>
                        <input type="hidden" name="gender" value="<?php echo $_POST['gender']; ?>">
                    </div>
                </div>

                <div class="flex flex-row space-x-3 w-full">
                    <div class="bg-[#70AD47] p-[2%] border-[1px] border-[#5e87ab] w-[30%]">
                        <label class="text-white">Phân khoa</label>
                    </div>
                    <div class='flex items-center justify-center'>
                        <label>
                            <?php echo $_POST['khoa'] ?>
                            <input type="hidden" name="khoa" value="<?php echo $_POST['khoa']; ?>">
                        </label>
                    </div>
                </div>

                <div class="flex flex-row space-x-3 w-full">
                    <div class="bg-[#70AD47] p-[2%] border-[1px] border-[#5e87ab] w-[30%]">
                        <label class="text-white">Ngày sinh</label>
                    </div>
                    <div class='flex items-center justify-center'>
                        <label>
                            <?php
                            $date = date_create_from_format("Y-m-d", $_POST['dateofbirth']);
                            $formattedDate = date_format($date, "d/m/Y");
                            echo $formattedDate
                                ?>
                        </label>
                        <input type="hidden" name="dateofbirth" value="<?php echo $_POST['dateofbirth']; ?>">
                    </div>
                </div>

                <div class="flex flex-row space-x-3 w-full">
                    <div class="bg-[#70AD47] p-[2%] border-[1px] border-[#5e87ab] w-[30%]">
                        <label class="text-white">Địa chỉ</label>
                    </div>
                    <div class='flex items-center justify-center'>
                        <label>
                            <?php echo $_POST['address'] ?>
                            <input type="hidden" name="address" value="<?php echo $_POST['address']; ?>">
                        </label>
                    </div>
                </div>

                <div class="flex flex-row space-x-3 w-full">
                    <div class="bg-[#70AD47] p-[2%] border-[1px] border-[#5e87ab] w-[30%]">
                        <label class="text-white">Hình ảnh</label>
                    </div>
                    <div class='flex items-center justify-center'>
                        <?php process_image(); ?>
                        <input type="hidden" name="avatar" id="avatar" value="<?php echo basename($_FILES["image"]["name"]); ?>">
                    </div>
                </div>

                <div class="flex justify-center">
                    <button type="submit"
                        class="bg-[#70AD47] border-[1px] border-[#5e87ab] text-white rounded-[10px] p-[2%] ">Xác
                        nhận</button>
                </div>
            </div>
        </form>
    </div>
</body>

</html>