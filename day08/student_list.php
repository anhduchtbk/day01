<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link rel="stylesheet" href="../style.css" />
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.12.9/umd/popper.min.js"></script>
    <script src="https://code.jquery.com/jquery-3.6.0.min.js"></script>
    <script src="https://cdn.tailwindcss.com"></script>
    <script>
        function searchStudents() {
            // Lấy giá trị của phần tử có id là 'department'
            var department = $('#major').val();
            var keyword = $('#keyword').val();
            console.log(department);
            // Kiểm tra nếu có ít nhất một trong hai giá trị department hoặc keyword tồn tại
            if (department || keyword) {
                // Sử dụng AJAX để gửi yêu cầu POST đến URL 'dashboard.php'
                $.ajax({
                    type: 'POST', // Phương thức yêu cầu là POST
                    url: 'student_list.php', // Đường dẫn tới tệp xử lý yêu cầu
                    data: { department, keyword }, //them gia tri vao data gui di
                    success: function (data) {
                        // Khi yêu cầu thành công, chạy hàm
                        // removeUnwantedForm để xử lý dữ liệu trả về
                        data = removeUnwantedForm(data);
                        // Thêm dữ liệu đã xử lý vào phần tử có id là 'studentTable'
                        $('#studentTable').html(data);
                    }
                });
            }
        }

        function resetForm() {
            // Sử dụng jQuery để lấy phần tử form có id là 'search-form'
            var form = $("#search-form")[0];

            // Sử dụng jQuery để thiết lập selectedIndex của phần tử 'department' trong form về 0
            $("select[name='department']", form).prop('selectedIndex', 0);

            // Sử dụng jQuery để thiết lập giá trị của phần tử 'search' trong form về chuỗi rỗng
            $("input[name='search']", form).val('');
        }


        function removeUnwantedForm(data) {
            var $temp = $('<div>').html(data);
            $temp.find('#form-typing').remove();
            return $temp.html();
        }
        $deleteModal = false;
        function openModal(studentId) {
            $('#deleteModal').modal('show');
        }

        function closeModal() {
            $('#deleteModal').modal('hide');
        }

        function deleteInfo(studentId) {
            // Thiết lập ID của sinh viên cần xóa trong trường ẩn
            $('#idToDelete').val(studentId);
            $deleteModal = true;
            openModal(studentId); // Mở modal trước khi xóa
        }

    </script>

</head>

<body>
    <?php
    include 'database.php';
    // $numStudentSql = "SELECT COUNT(*) AS count FROM students;";
    // $numStudent = $conn->query($numStudentSql);
    
    // $studentSql = "SELECT `ID`, `HoTen`, `Khoa` FROM students;";
    // $studentSql = $conn->query($studentSql);
    // $numStudentRow = $numStudent->fetch_assoc();
    $NumStudentSql = "SELECT COUNT(*) AS count FROM students;";
    $numStudent = $conn->query($NumStudentSql);
    $numStudentRow = $numStudent->fetch_assoc();

    ?>


    <?php
    $major = array(
        "MAT" => "Khoa học máy tính",
        "KDL" => "Khoa học vật liệu"
    );
    ?>
    <div class="w-screen px-[15%]" id="form-typing">
        <div class="w-full h-full space-y-3">
            <form id="search-form" action="student_list.php" method="post">
                <div class="grid grid-cols-3">
                    <label>
                        Khoa
                    </label>

                    <select id="major" name="major" class="border-[1px] bg-[#e2eaf4] border-[#6e83a9] w-full col-span-2"
                        onchange="searchStudents()">
                        <option value="cpk" selected>Chọn Phân Khoa</option>
                        <?php
                        foreach ($major as $key => $value) {
                            echo "<option value=\"$key\">$value</option>";
                        }
                        ?>
                    </select>
                </div>

                <div class="grid grid-cols-3">
                    <label>
                        Từ khóa
                    </label>

                    <input type="text" id="keyword" class="border-[1px] bg-[#e2eaf4] border-[#6e83a9] col-span-2"
                        onkeyup="searchStudents()" />
                </div>
            </form>

            <div class="w-full flex justify-center">
                <button class="search-button bg-[#4e82bd] p-4 rounded-lg text-white" id='resetbt'>
                    Reset
                </button>
            </div>
        </div>
    </div>

    <div class='w-screen'>
        <div id="studentTable" class="w-fit">
            <div class="flex flex-row justify-between">
                <p>Số sinh viên tìm thấy:
                    <?php echo $numStudentRow['count'] ?>
                </p>

                <button class="bg-[#4e82bd] p-4 rounded-lg text-white"><a href="register.php">Thêm</a></button>
            </div>

            <table class='w-fit'>
                <tr>
                    <td class='id'>No</td>
                    <td class='name'>Tên Sinh Viên</td>
                    <td class='department'>Khoa</td>
                    <td class='action'>Action</td>
                </tr>

                <!-- <?php
                // while ($row = $studentSql->fetch_assoc()) {
                //     echo "<tr>";
                //     echo "<td class='id text-black'>" . $row['ID'] . "</td>";
                //     echo "<td class='name'>" . $row['HoTen'] . "</td>";
                //     echo "<td class='department'>" . $row['Khoa'] . "</td>";
                //     echo "<td class='action'>            
                //     <button class='bg-[#4e82bd] text-white rounded-lg'><a href='#' name='delete'>Xóa</a></button>
                //     <button class='bg-[#4e82bd] text-white rounded-lg'><a href='#' name='edit'>Sửa</a></button>
                // </td>";
                // }
                ?> -->

                <?php
                $tableContent = '';
                $searchSql = "";
                if ($_SERVER["REQUEST_METHOD"] == "POST") {

                    $department = $_POST['department'];
                    $keyword = $_POST['keyword'];
                    $selectedDepartment = $_POST["department"];
                    if ($selectedDepartment == "cpk") {
                        $selectedDepartment = "";
                    } elseif ($selectedDepartment == "MAT") {
                        $selectedDepartment = "Khoa học máy tính";
                    } elseif ($selectedDepartment == "KDL") {
                        $selectedDepartment = "Khoa học vật liệu";
                    }
                    if ($selectedDepartment) {
                        $searchSql = " SELECT * FROM `students` WHERE `Khoa` LIKE '$selectedDepartment' AND (`ID` LIKE '%$keyword%' OR `HoTen` LIKE '%$keyword%')";
                    } else {
                        $searchSql = " SELECT * FROM `students` WHERE `Khoa` LIKE '%$keyword%' OR `ID` LIKE '%$keyword%' OR `HoTen` LIKE '%$keyword%'";
                    }
                    ;
                } else {
                    $searchSql = "SELECT * FROM `students`";
                }
                $studentSql = $conn->query($searchSql);
                while ($row = $studentSql->fetch_assoc()) {
                    $id = $row['ID'];
                    echo "<tr>";
                    echo "<td class='id text-black'>" . $row['ID'] . "</td>";
                    echo "<td class='name'>" . $row['HoTen'] . "</td>";
                    echo "<td class='department'>" . $row['Khoa'] . "</td>";
                    echo "<td class='flex'>     
                <form method='POST'>       
                <input type='hidden' name='id' value='" . $row["ID"] . "'>
                <button class='bg-[#4e82bd] text-white rounded-lg' type='button' onclick='deleteInfo(" . $row["ID"] . ")'> Xóa </button>
                </form>
                <form method='post'>
                <input type='hidden' name='id' value='" . $row["ID"] . "'>
                <input type='hidden' name = 'name' value= '" . $row["HoTen"] . "'>
                <input type='hidden' name = 'gender' value= '" . $row["GioiTinh"] . "'>
                <input type='hidden' name = 'department' value= '" . $row["Khoa"] . "'>
                <input type='hidden' name = 'birthday' value= '" . $row["NgaySinh"] . "'>
                <input type='hidden' name = 'address' value= '" . $row["DiaChi"] . "'>
                <input type='hidden' name = 'image' value= '" . $row["Anh"] . "'>
                <button class='bg-[#4e82bd] text-white rounded-lg' type='submit' name='edit'>Sửa</button>
                </form>
            </td>";

                }
                ?>

            </table>
        </div>
    </div>
    <script>
        document.getElementById("resetbt").addEventListener('click', function () {
            document.getElementById("keyword").value = "";
            document.getElementById("major").value = "cpk";
        });
    </script>
</body>

</html>