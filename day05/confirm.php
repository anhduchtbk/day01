<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link rel="stylesheet" href="../style.css" />
    <script src="https://code.jquery.com/jquery-3.6.0.min.js"></script>
    <script src="https://cdn.jsdelivr.net/jquery.validation/1.16.0/jquery.validate.min.js"></script>
</head>

<body>
    <?php
    include "./image_process.php";
    $data = $_COOKIE["data"];
    $result = json_decode($data, true);
    ?>
    <div class="w-screen h-screen px-[30%] py-[10%]">
        <div class="border-[1px] border-[#5e87ab] p-[5%] space-y-2 w-full">
            <div class="flex flex-row space-x-3 w-full">
                <div class="bg-[#70AD47] p-[2%] border-[1px] border-[#5e87ab] w-[30%]">
                    <label class="text-white">Họ và tên </label>
                </div>
                <div class='flex items-center justify-center'>
                    <label>
                        <?php echo $result['fullname'] ?>
                    </label>
                </div>
            </div>

            <div class="flex flex-row space-x-3 w-full">
                <div class="bg-[#70AD47] p-[2%] border-[1px] border-[#5e87ab] w-[30%]">
                    <label class="text-white">Giới tính </label>
                </div>
                <div class='flex items-center justify-center'>
                    <label>
                        <?php echo $result['gender'] ?>
                    </label>
                </div>
            </div>

            <div class="flex flex-row space-x-3 w-full">
                <div class="bg-[#70AD47] p-[2%] border-[1px] border-[#5e87ab] w-[30%]">
                    <label class="text-white">Phân khoa</label>
                </div>
                <div class='flex items-center justify-center'>
                    <label>
                        <?php echo $result['major'] ?>
                    </label>
                </div>
            </div>

            <div class="flex flex-row space-x-3 w-full">
                <div class="bg-[#70AD47] p-[2%] border-[1px] border-[#5e87ab] w-[30%]">
                    <label class="text-white">Ngày sinh</label>
                </div>
                <div class='flex items-center justify-center'>
                    <label>
                        <?php
                        $date = date_create_from_format("Y-m-d", $result['birthdate']);
                        $formattedDate = date_format($date, "d/m/Y");
                        echo $formattedDate
                            ?>
                    </label>
                </div>
            </div>

            <div class="flex flex-row space-x-3 w-full">
                <div class="bg-[#70AD47] p-[2%] border-[1px] border-[#5e87ab] w-[30%]">
                    <label class="text-white">Địa chỉ</label>
                </div>
                <div class='flex items-center justify-center'>
                    <label>
                        <?php echo $result['address'] ?>
                    </label>
                </div>
            </div>

            <div class="flex flex-row space-x-3 w-full">
                <div class="bg-[#70AD47] p-[2%] border-[1px] border-[#5e87ab] w-[30%]">
                    <label class="text-white">Hình ảnh</label>
                </div>
                <div class='flex items-center justify-center'>
                    <?php process_image(); ?>
                </div>
            </div>

            <div class="flex justify-center">
                <button type="submit"
                    class="bg-[#70AD47] border-[1px] border-[#5e87ab] text-white rounded-[10px] p-[2%] ">Xác
                    nhận</button>
            </div>
        </div>
    </div>
</body>

</html>