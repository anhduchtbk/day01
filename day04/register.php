<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Document</title>
    <link rel="stylesheet" href="../style.css" />
    <script src="https://code.jquery.com/jquery-3.6.0.min.js"></script>
    <script src="https://cdn.jsdelivr.net/jquery.validation/1.16.0/jquery.validate.min.js"></script>
    <style>
        .error {
            color: red;
        }
    </style>
</head>

<body>
    <?php
    $sex = array(
        0 => "Nam",
        1 => "Nữ"
    );
    $major = array(
        "MAT" => "Khoa học máy tính",
        "KDL" => "Khoa học vật liệu"
    );
    ?>

    <div class="w-screen h-screen px-[30%] py-[10%]">
        <form id="myform">
            <div class="border-[1px] border-[#5e87ab] p-[5%] space-y-2 w-full">
                <div id="errorMessage">

                </div>
                <div class="flex flex-row space-x-3 justify-between w-full">
                    <div class="bg-[#5b9bd5] p-[2%] border-[1px] border-[#5e87ab] w-[30%]">
                        <label class="text-white">Họ và tên <span class='text-[#ff0000]'>*</span></label>
                    </div>
                    <input id="fullname" class="col-span-2 border-[1px] border-[#5b9bd5] w-[70%]" />
                </div>

                <div class="flex flex-row space-x-3 justify-between">
                    <div class="bg-[#5b9bd5] p-[2%] border-[1px] border-[#5e87ab] w-[30%]">
                        <label class="text-white">Giới tính <span class='text-[#ff0000]'>*</span></label>
                    </div>
                    <div class="col-span-2 flex flex-row space-x-2 items-center w-[70%]">
                        <?php
                        for ($i = 0; $i < count($sex); $i++) {
                            echo "<div>
                                    <input class='accent-green bg-blue text-black' type=\"radio\" name=\"gender\" value=\"$sex[$i]\" id=\'gender\'>
                                    <label>$sex[$i]</label>
                                </div>";
                        }
                        ?>
                    </div>
                </div>

                <div class="flex flex-row space-x-3 justify-between">
                    <div class="bg-[#5b9bd5] p-[2%] border-[1px] border-[#5e87ab] w-[30%]">
                        <label class="text-white">Phân khoa <span class='text-[#ff0000]'>*</span></label>
                    </div>
                    <select id="major" name="khoa" class="border-[1px] border-[#5e87ab] appearance-none w-[70%]">
                        <option value="cpk" selected>Chọn Phân Khoa</option>
                        <?php
                        foreach ($major as $key => $value) {
                            echo "<option value=\"$key\">$value</option>";
                        }
                        ?>
                    </select>
                </div>

                <div class="flex flex-row space-x-3 justify-between">
                    <div class="bg-[#5b9bd5] p-[2%] border-[1px] border-[#5e87ab] w-[30%]">
                        <label class="text-white">Ngày sinh <span class='text-[#ff0000]'>*</span></label>
                    </div>
                    <input id="birthdate" class="col-span-2 border-[1px] border-[#5b9bd5] w-[70%]" type="date" />
                </div>

                <div class="flex flex-row space-x-3 justify-between">
                    <div class="bg-[#5b9bd5] p-[2%] border-[1px] border-[#5e87ab] h-fit w-[30%]">
                        <label class="text-white">Địa chỉ</label>
                    </div>
                    <input class="col-span-2 border-[1px] border-[#5b9bd5] w-[70%] min-h-[70px]" />
                </div>

                <div class="flex justify-center">
                    <button type="submit"
                        class="bg-[#70AD47] border-[1px] border-[#5e87ab] text-white rounded-[10px] p-[2%] ">Đăng
                        ký</button>
                </div>
            </div>
        </form>

        <!-- Xử lý dữ liệu bằng js -->
        <script>
            $(document).ready(function () {
                $('#myform').submit(function (event) {
                    event.preventDefault();
                    var errors = [];

                    var name = $('#fullname').val();
                    if (name.trim() === '') {
                        errors.push('Hãy nhập tên.');
                    }

                    var gender = $('input[name="gender"]:checked').val();
                    if (!gender) {
                        errors.push('Hãy chọn giới tính');
                    }

                    var major = $('#major').val();
                    if (major === 'cpk') {
                        errors.push('Hãy chọn phân khoa.');
                    }

                    var birthdate = $('#birthdate').val();
                    if (birthdate.trim() === '') {
                        errors.push('Hãy nhập ngày sinh');
                    }

                    if (errors.length >= 0) {
                        displayErrors(errors);
                    }
                });

                function displayErrors(errors) {
                    var errorContainer = $('#errorMessage');
                    errorContainer.empty();
                    for (var i = 0; i < errors.length; i++) {
                        var errorMessage = $('<div>').text(errors[i]).addClass('error');
                        errorContainer.append(errorMessage);
                    }
                }
            });
        </script>
    </div>
</body>

</html>