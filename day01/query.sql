-- Tạo Database tên QLSV
CREATE DATABASE QLSV

-- Tạo bảng tên DMKHOA
CREATE TABLE DMKHOA (
    MaKH varchar(6) PRIMARY KEY,
    TenKhoa varchar(30)
)

-- Tạo bảng tên SINHVIEN
CREATE TABLE SINHVIEN (
    MaSV varchar(6) PRIMARY KEY,
    HoSV varchar(30),
    TenSV varchar(15),
    GioiTinh char(1),
    NgaySinh DATETIME,
    NoiSinh varchar(50),
    DiaChi varchar(50),
    MaKH varchar(6),
    HocBong int
)

-- Lấy danh sách các sinh viên có tên khoa 'Công nghệ thông tin'
SELECT B.HoSV, B.TenSV FROM DMKHOA A, SINHVIEN B
WHERE A.MaKH = B.MaKH AND A.TenKhoa = "Công nghệ thông tin"

