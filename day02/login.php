<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link href="../style.css" rel="stylesheet">
</head>

<body>
    <?php
    function getTime()
    {
        date_default_timezone_set('Asia/Ho_Chi_Minh');
        return date('H:i');
    }

    function getRealDate()
    {
        date_default_timezone_set('Asia/Ho_Chi_Minh');
        return date('Y/m/d');
    }
    ?>

    <div class="px-[30%] grid items-center h-screen">
        <div class="border-[2px] border-[#5e87ab] space-y-2 p-[10%]">
            <div class="bg-[#f2f2f2] p-[2%]">
                <p>
                    Bây giờ là:
                    <?php
                    echo getTime();
                    ?>,&nbsp;
                    thứ
                    <?php
                    echo date('w', strtotime(getRealDate()) + 1);
                    ?>&nbsp;
                    ngày
                    <?php
                    date_default_timezone_set('Asia/Ho_Chi_Minh');
                    echo date('d/m/Y');
                    ?>
                </p>
            </div>


            <!-- Đăng nhập -->
            <div class="grid grid-cols-2 gap-2">
                <div class="p-[1%] bg-[#5b9bd5] border-[2px] border-[#5e87ab]">
                    <p class="text-white">
                        Tên đăng nhập
                    </p>
                </div>
                <input class="border-[2px] border-[#5e87ab]" />
            </div>
            <!-- Mật khẩu -->
            <div class="grid grid-cols-2 gap-2">
                <div class="p-[1%] bg-[#5b9bd5] border-[2px] border-[#5e87ab]">
                    <p class="text-white">
                        Mật khẩu
                    </p>
                </div>
                <input class="border-[2px] border-[#5e87ab]" />
            </div>

            <div class="grid items-center px-[25%]">
                <button class="bg-[#5b9bd5] rounded-[10px] text-white py-[1%]">
                    Đăng nhập
                </button>
            </div>
        </div>

    </div>
</body>

</html>