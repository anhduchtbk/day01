<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Document</title>
    <link rel="stylesheet" href="../style.css" />
    <script src="https://code.jquery.com/jquery-3.6.0.min.js"></script>
    <script src="https://cdn.jsdelivr.net/jquery.validation/1.16.0/jquery.validate.min.js"></script>
    <style>
        .error {
            color: red;
        }
    </style>
</head>

<body>
    <?php
    $city_HN = [
        0 => "",
        1 => "Hoàng Mai",
        2 => "Thanh Trì",
        3 => "Nam Từ Liêm",
        4 => "Hà Đông",
        5 => "Cầu giấy"
    ];

    $city_HCM = [
        0 => "",
        1 => "Quận 1",
        2 => "Quận 2",
        3 => "Quận 3",
        4 => "Quận 7",
        5 => "Quận 9"
    ];

    $city = [
        0 => "Hà Nội",
        1 => "Hồ Chí Minh"
    ]
        ?>

    <script>
        $(document).ready(function () {
            // Lắng nghe sự kiện onchange của select city
            $("#city").change(function () {
                var selectedCity = $(this).val(); // Lấy giá trị city đã chọn
                var districtSelect = $("#district"); // Lấy select element của district

                // Xóa các option hiện tại trong select district
                districtSelect.empty();

                // Kiểm tra giá trị city đã chọn
                if (selectedCity === "Hà Nội") {
                    // Nếu city là Hà Nội, thêm các option từ mảng city_HN
                    $.each(<?php echo json_encode($city_HN); ?>, function (key, value) {
                        districtSelect.append($('<option></option>').val(value).html(value));
                    });
                } else if (selectedCity === "Hồ Chí Minh") {
                    // Nếu city là Hồ Chí Minh, thêm các option từ mảng city_HCM
                    $.each(<?php echo json_encode($city_HCM); ?>, function (key, value) {
                        districtSelect.append($('<option></option>').val(value).html(value));
                    });
                }

                // Kích hoạt hoặc vô hiệu hóa select district tùy thuộc vào giá trị city đã chọn
                if (selectedCity !== "") {
                    districtSelect.prop('disabled', false);
                } else {
                    districtSelect.prop('disabled', true);
                }
            });
        });
    </script>
    <form method="post" id="registrationForm" action="regist_student.php">
        <div class="w-screen h-screen px-[30%] flex flex-col justify-center items-center space-y-3">
            <h1>Form đăng ký sinh viên</h1>
            <div id="errorMessages" class="error">
                <script>
                    $(document).ready(function () {
                        $("#registerButton").click(function () {
                            var name = $("#fullname").val();
                            var gender = $("input[name='gender']:checked").val();
                            var year = $("#year").val();
                            var month = $("#month").val();
                            var day = $("#day").val();
                            var add1 = $("#city").val();
                            var add2 = $("#district").val();

                            var errorMessages = [];

                            if (name === "") {
                                errorMessages.push("Hãy nhập họ tên.");
                            }

                            if (!gender) {
                                errorMessages.push("Hãy chọn giới tính.");
                            }

                            if (year === "" || month === "" || day === "") {
                                errorMessages.push("Hãy chọn ngày sinh");
                            }
                            
                            if(add1 === "" || add2 === "") {
                                errorMessages.push("Hãy chọn địa chỉ");
                            }

                            if (errorMessages.length > 0) {
                                var errorMessageHtml = "";
                                for (var i = 0; i < errorMessages.length; i++) {
                                    errorMessageHtml += errorMessages[i] + "<br>";
                                }
                                $("#errorMessages").html(errorMessageHtml);
                            } else {
                                $("#errorMessages").html("");
                            }
                        });
                    });
                </script>
            </div>

            <div class="w-full h-fit">
                <div class="flex flex-row">
                    <label class="text-black bg-slate-400 w-[30%]">Họ và tên</label>
                    <input type="text" id="fullname" name="fullname" required />
                </div>

                <div class="flex flex-row">
                    <label class="text-black bg-slate-400 w-[30%]">Giới tính</label>
                    <div class="flex flex-row">
                        <input type="radio" name="gender" value="nam" required>Nam</input>
                        <input type="radio" name="gender" value="nữ" required>Nữ</input>
                    </div>
                </div>

                <div class="flex flex-row space-x-3">
                    <label class="text-black bg-slate-400 w-[30%]">Ngày sinh</label>
                    <div class="flex flex-row">
                        <div class="flex flex-row">
                            <label>Năm</label>
                            <select id="year" name="year" required>
                                <option value=""></option>
                                <?php
                                for ($i = 1983; $i <= 2008; $i++) {
                                    echo "<option value=\"$i\">$i</option>";
                                }
                                ?>
                            </select>
                        </div>

                        <div class="flex flex-row">
                            <label>Tháng</label>
                            <select id="month" name="month" required>
                                <option value=""></option>
                                <?php
                                for ($i = 1; $i <= 12; $i++) {
                                    echo "<option value=\"$i\">$i</option>";
                                }
                                ?>
                            </select>
                        </div>

                        <div class="flex flex-row">
                            <label>Ngày</label>
                            <select name="day" id="day" required>
                                <option value=""></option>
                                <?php
                                for ($i = 1; $i <= 31; $i++) {
                                    echo "<option value=\"$i\">$i</option>";
                                }
                                ?>
                            </select>
                        </div>
                    </div>
                </div>

                <div class="flex flex-row">
                    <label class="text-black bg-slate-400 w-[30%]">Địa chỉ</label>
                    <div class="flex flex-row">
                        <label>Thành phố</label>
                        <select id="city" name="city" required>
                            <option value=""></option>
                            <?php
                            foreach ($city as $key => $value) {
                                echo "<option value=\"$value\">$value</option>";
                            }

                            ?>
                        </select>
                    </div>

                    <div class="flex flex-row">
                        <label>Quận</label>
                        <select id="district" name="district" disabled required>

                        </select>
                    </div>
                </div>

                <div class="flex flex-row">
                    <label class="text-black bg-slate-400 w-[30%]">Thông tin khác</label>
                    <textarea name="otherinfor"></textarea>
                </div>

                <div class="w-full flex justify-center" id="registerButton" name="registerButton">
                    <button type="submit" name="signup"
                        class="border-[1px] border-[#5e87ab] bg-slate-300 text-white rounded-[10px] p-[2%] m-5 w-fit">Đăng
                        ký</button>
                </div>

            </div>
        </div>
    </form>
</body>

</html>