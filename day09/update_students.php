<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Document</title>
    <link rel="stylesheet" href="../style.css" />
    <script src="https://code.jquery.com/jquery-3.6.0.min.js"></script>
    <script src="https://cdn.jsdelivr.net/jquery.validation/1.16.0/jquery.validate.min.js"></script>
    <script src="https://cdn.tailwindcss.com"></script>
    <style>
        .error {
            color: red;
        }
    </style>
</head>

<body>
    <?php
    ob_start();
    $sex = array(
        0 => "Nam",
        1 => "Nữ"
    );
    $major = array(
        "MAT" => "Khoa học máy tính",
        "KDL" => "Khoa học vật liệu"
    );
    ?>

    <div class="w-screen h-screen px-[30%] py-[10%]">
        <form id="myform" action="update_students.php" enctype="multipart/form-data" method="post">
            <input type="text" id="id" name="id" class="col-span-2 border-[1px] border-[#5b9bd5] w-[70%]" hidden
                value='<?php echo $_POST['id']; ?>' />
            <div class="border-[1px] border-[#5e87ab] p-[5%] space-y-2 w-full">
                <div id="errorMessage">

                </div>
                <div class="flex flex-row space-x-3 justify-between w-full">
                    <div class="bg-[#70AD47] p-[2%] border-[1px] border-[#5e87ab] w-[30%]">
                        <label class="text-white">Họ và tên <span class='text-[#ff0000]'>*</span></label>
                    </div>
                    <input type="text" id="fullname" name="fullname"
                        class="col-span-2 border-[1px] border-[#5b9bd5] w-[70%]"
                        value='<?php echo $_POST['name']; ?>' />
                </div>

                <div class="flex flex-row space-x-3 justify-between">
                    <div class="bg-[#70AD47] p-[2%] border-[1px] border-[#5e87ab] w-[30%]">
                        <label class="text-white">Giới tính <span class='text-[#ff0000]'>*</span></label>
                    </div>
                    <div class="col-span-2 flex flex-row space-x-2 items-center w-[70%]">
                        <?php
                        for ($i = 0; $i < count($sex); $i++) {
                            if ($sex[$i] == $_POST['gender']) {
                                echo "<div>
                                    <input class='accent-green bg-blue text-black' type=\"radio\" name=\"gender\" value=\"$sex[$i]\" checked>
                                    <label>$sex[$i]</label>
                                </div>";
                            } else {
                                echo "<div>
                                <input class='accent-green bg-blue text-black' type=\"radio\" name=\"gender\" value=\"$sex[$i]\">
                                <label>$sex[$i]</label>
                                    </div>";
                            }
                        }

                        ?>
                    </div>
                </div>

                <div class="flex flex-row space-x-3 justify-between">
                    <div class="bg-[#70AD47] p-[2%] border-[1px] border-[#5e87ab] w-[30%]">
                        <label class="text-white">Phân khoa <span class='text-[#ff0000]'>*</span></label>
                    </div>
                    <select id="major" name="khoa" class="border-[1px] border-[#5e87ab] appearance-none w-[70%]">
                        <?php
                        foreach ($major as $key => $value) {
                            if ($value == $_POST['department']) {
                                echo "<option value=\"$key\" selected>$value</option>";
                            } else {
                                echo "<option value=\"$key\">$value</option>";
                            }
                        }
                        ?>
                    </select>
                </div>

                <div class="flex flex-row space-x-3 justify-between">
                    <div class="bg-[#70AD47] p-[2%] border-[1px] border-[#5e87ab] w-[30%]">
                        <label class="text-white">Ngày sinh <span class='text-[#ff0000]'>*</span></label>
                    </div>

                    <input id="birthdate" name="dateofbirth" class="col-span-2 border-[1px] border-[#5b9bd5] w-[70%]"
                        type="date" />
                    <?php
                    // Xử lý PHP
                    $birthdate = new DateTime($_POST['birthday']);
                    echo '<script>document.getElementById("birthdate").value = "' . $birthdate->format('Y-m-d') . '";</script>';
                    ?>
                </div>

                <div class="flex flex-row space-x-3 justify-between">
                    <div class="bg-[#70AD47] p-[2%] border-[1px] border-[#5e87ab] h-fit w-[30%]">
                        <label class="text-white">Địa chỉ</label>
                    </div>

                    <input id='address' name='address'
                        class="col-span-2 border-[1px] border-[#5b9bd5] w-[70%] min-h-[70px]"
                        value='<?php echo $_POST['address']; ?>' />
                </div>

                <div class="flex flex-row space-x-3 justify-between items-center">
                    <div class="bg-[#70AD47] p-[2%] border-[1px] border-[#5e87ab] h-fit w-[30%]">
                        <label class="text-white">Hình ảnh</label>
                    </div>
                    <input type='file' id='image' name='image' class="w-[70%]" value="" />
                </div>

                <div class="flex justify-center">
                    <button class="bg-[#70AD47] border-[1px] border-[#5e87ab] text-white rounded-[10px] p-[2%] ">Xác
                        nhận
                        <input type='hidden' name='confirmEdit' value='true'>
                    </button>
                </div>

            </div>
        </form>
        <?php
        if ($_SERVER["REQUEST_METHOD"] == "POST" && isset($_POST["confirmEdit"])) {
            $gender = $_POST["gender"];
            $id = $_POST["id"];
            $birthday = $_POST["dateofbirth"];
            $department = $_POST["khoa"];
            $address = $_POST["address"];
            $name = $_POST["fullname"];
            $insertDepartment = ($department == "MAT") ? "Khoa học máy tính" : "Khoa học vật liệu";

            include('database.php'); // Bao gồm tệp kết nối cơ sở dữ liệu
            // Sử dụng prepared statement để tránh SQL injection
            $stmt = $conn->prepare("UPDATE students SET HoTen = ?, GioiTinh = ?, NgaySinh = ?, Khoa = ?, DiaChi = ?, Anh = ' ' WHERE ID = ?");
            $stmt->bind_param("ssssss", $name, $gender, $birthday, $insertDepartment, $address, $id);

            if ($stmt->execute()) {
                header("Location: student_list.php");
                die(); // Đảm bảo dừng việc thực thi mã nguồn ngay sau hàm header
            } else {
                echo "Lỗi: " . $stmt->error;
            }

            $stmt->close();
            $conn->close();
        }
        ob_end_flush();
        ?>
    </div>


</body>

</html>