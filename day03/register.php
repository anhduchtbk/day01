<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Document</title>
    <link rel="stylesheet" href="../style.css" />
</head>

<body>
    <?php
    $sex = array(
        0 => "Nam",
        1 => "Nữ"
    );
    $major = array(
        "MAT" => "Khoa học máy tính",
        "KDL" => "Khoa học vật liệu"
    );
    ?>
    <div class="w-screen h-screen grid justify-center items-center px-[30%]">
        <form>
            <div class="border-[1px] border-[#5e87ab] p-[5%] space-y-2">
                <div class="grid grid-cols-3 gap-3">
                    <div class="bg-[#5b9bd5] p-[2%] border-[1px] border-[#5e87ab]">
                        <label class="text-white">Họ và tên</label>
                    </div>
                    <input class="col-span-2 border-[1px] border-[#5b9bd5] min-w" />
                </div>

                <div class="grid grid-cols-3 gap-3">
                    <div class="bg-[#5b9bd5] p-[2%] border-[1px] border-[#5e87ab]">
                        <label class="text-white">Giới tính</label>
                    </div>
                    <div class="col-span-2 flex flex-row space-x-2 items-center">
                        <?php
                        for ($i = 0; $i < count($sex); $i++) {
                            echo "<div>
                                <input type=\"radio\" name=\"sex\" value=\"$sex[$i]\" id=\'girl\' class=\"text-black\">
                                <label>$sex[$i]</label>
                            </div>";
                        }
                        ?>
                    </div>
                </div>

                <div class="grid grid-cols-3 gap-3">
                    <div class="bg-[#5b9bd5] p-[2%] border-[1px] border-[#5e87ab]">
                        <label class="text-white">Họ và tên</label>
                    </div>
                    <select id="khoa" name="khoa" class="border-[1px] border-[#5e87ab] appearance-none min-w-fit">
                        <option value="cpk" selected>Chọn Phân Khoa</option>
                        <?php
                        foreach ($major as $key => $value) {
                            echo "<option value=\"$key\">$value</option>";
                        }
                        ?>
                    </select>
                </div>

                <div class="flex justify-center">
                    <button class="bg-[#70AD47] border-[1px] border-[#5e87ab] text-white rounded-[10px] p-[2%] ">Đăng
                        ký</button>
                </div>
            </div>
        </form>
    </div>
</body>

</html>